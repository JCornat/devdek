CREATE DATABASE  IF NOT EXISTS `devdek` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `devdek`;
-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: devdek
-- ------------------------------------------------------
-- Server version	5.5.38-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Article`
--

DROP TABLE IF EXISTS `Article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `summary` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_CD8737FA12469DE2` (`category_id`),
  CONSTRAINT `FK_CD8737FA12469DE2` FOREIGN KEY (`category_id`) REFERENCES `Category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Article`
--

LOCK TABLES `Article` WRITE;
/*!40000 ALTER TABLE `Article` DISABLE KEYS */;
INSERT INTO `Article` VALUES (1,1,'Hello World!','2015-01-18 00:00:00','Et voici mon premier article !','<p>Quelle joie pour moi <3</p>',1),(2,1,'Ceci est un test !','2015-01-18 00:00:00','J\'espère que vous allez apprécier !','<p>BLABLABLA</p>',1),(3,1,'AWS, le bonheur des intégrateurs en herbe','2015-01-19 01:23:00','1 an de serveur offert par Amazon pour héberger toutes ses applications, si ça c\'est pas la belle vie. Pour configurer tout ça, c\'est par ici !','<h3>AWS ?</h3>\r\n\r\n<p>Comprenez Amazon Web Service, un service d&#39;Amazon pour profiter de leur&nbsp;serveurs, comprenant une ribambelle d&#39;applications, dont EC2, le service qui va nous int&eacute;resser.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>EC2</h3>\r\n\r\n<p>Le service qui va vous permettre de lancer des serveurs dans le monde entier (vraiment partout)&nbsp;pour h&eacute;berger vos sites, et o&ugrave; vous pourrez y acc&eacute;der par acc&egrave;s SSH et avoir tous les droits dessus.</p>\r\n\r\n<p>Vous avez des images d&#39;OS a disposition (Ubuntu, Debian,&nbsp;RedHat, CentOS, Windows), vous lancez celle qui vous plait, et votre serveur est pr&ecirc;t dans la minute avec adresse IP, url d&#39;acc&egrave;s, l&#39;OS qui tourne, et 3Go de stockage !</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>AWS Free Usage Tier</h3>\r\n\r\n<p>C&#39;est l&#39;offre d&#39;Amazon qui va vous permettre d&#39;acc&eacute;der &agrave;une petite dizaine d&#39;applications gratuitement durant 1 an, sans engagement, et qui comprend un serveur... EC2 &eacute;videmment !</p>\r\n\r\n<p>Ici le lien pour y acc&eacute;der vite fait et profiter :&nbsp;<a href=\"http://aws.amazon.com/free\" style=\"color: rgb(17, 102, 187); text-decoration: none; cursor: pointer; font-family: Arial, \'Droid Sans\', sans-serif; font-size: 14px; line-height: normal;\" target=\"_blank\">AWS Free Usage Tier</a>.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>Configuration sous Ubuntu</h3>\r\n\r\n<p>Bon, comme j&#39;aime bien Ubuntu, mon serveur tourne dessus, et du coup&nbsp;je voici la config pour cr&eacute;er ses cl&eacute;s ssh pour cloner ses repos sur le serveur et mettre &agrave; disposition facilement ses applications !</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Partie un :</p>\r\n\r\n<blockquote>\r\n<pre>\r\nsudo apt-get install php5 apache2 libapache2-mod-php5 mysql-server php5-mysql\r\nsudo apt-get install php5\r\nsudo add-apt-repository ppa:ondrej/php5-5.6\r\nsudo apt-get update\r\nsudo apt-get install python-software-properties\r\nsudo apt-get update\r\nsudo apt-get install php5 apache2 libapache2-mod-php5 mysql-server php5-mysql\r\nsudo service apache2 restart\r\nhistory\r\nsudo apt-get install git\r\ngit status\r\ncd /var/www/\r\nls\r\nsudo nano index.html\r\ncd /etc/apache2/sites-available/\r\nls\r\ncat 000-default.conf\r\nsudo nano coda.conf\r\ncd /var/www\r\nmkdir coda\r\nsudo mkdir coda\r\ncd coda/\r\nls\r\nll\r\nchown ubuntu:ubuntu .\r\nsudo chown ubuntu:ubuntu .\r\nll\r\nls\r\ngit init\r\nll\r\nnano index.html\r\nhistory\r\ncd /etc/apache2/sites-available/\r\nnano coda.conf\r\nsudo nano coda.conf\r\nsudo nano 000-default.conf\r\nsudo service apache2 restart\r\ncd /var/www/coda\r\nls\r\nll\r\ngit remote add origin git@bitbucket.org:JCornat/coda.git\r\nrm index.html\r\ngit pull\r\nssh-keygen\r\ncat ~/.ssh/id_rsa.pub\r\ngit pull\r\ngit pull origin master\r\nls\r\ntail -f /var/log/apache2/error.log\r\ncd ..\r\nsudo curl -sS https://getcomposer.org/installer | php\r\ncurl -sS https://getcomposer.org/installer | php\r\nsudo curl -sS https://getcomposer.org/installer | php\r\nhistory\r\n</pre>\r\n</blockquote>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Normalement, vous acc&eacute;dez &agrave; l&#39;url de votre serveur et tout roule !</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>See ya !</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>++</p>',1);
/*!40000 ALTER TABLE `Article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Category`
--

DROP TABLE IF EXISTS `Category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Category`
--

LOCK TABLES `Category` WRITE;
/*!40000 ALTER TABLE `Category` DISABLE KEYS */;
INSERT INTO `Category` VALUES (1,'Test');
/*!40000 ALTER TABLE `Category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_2DA1797792FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_2DA17977A0D96FBF` (`email_canonical`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES (3,'Jacques Cornat','2/BsU9mTwmuLoR5T+TxEEy3qvdQdEgcSTW4bo/gDssthSBnuASQGyjKN/5GClc3t7X3S/nI3L9ijSIS+kf5j/Q==','444b0avxflesgk8wc0kkokco444wg08','a:1:{i:0;s:11:\"ROLE_AUTHOR\";}','jacques cornat','cornat.jacques@gmail.com','cornat.jacques@gmail.com',1,'2015-01-19 01:23:17',0,0,NULL,NULL,NULL,0,NULL),(5,'aurora','PAW7AGyfqftBaq+NuK/MT7ADse3HgjPsgNz2jedt8P+mPOEv9jPabj0Enu2OTLXw8H9SoJaVzUyCZNAP/iffAg==','k3u6o885njkskcc8g48w8w4s8wg8k8o','a:0:{}','aurora','test@test.com','test@test.com',1,'2015-01-18 17:43:12',0,0,NULL,NULL,NULL,0,NULL);
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-01-19  1:37:44
