<?php

namespace App\Bundle\UserBundle\Controller;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use FOS\UserBundle\Model\UserInterface;

class UserController extends Controller {

    public function profileAction() {
        if(!$this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw new AccessDeniedException('Vous devez être connecté');
        }

        $user = $this->get('security.context')->getToken()->getUser();
        return $this->render('AppUserBundle:User:profile.html.twig', array(
            'user' => $user
        ));
    }

    public function detailAction($id) {
        $em = $this->getDoctrine()->getManager();
        $user = $em->find('AppUserBundle:User', $id);
        if(!$user) {
            return $this->redirect($this->generateUrl('blog_home'));
        }
        return $this->render('AppUserBundle:User:detail.html.twig', array(
            'user' => $user
        ));
    }

    public function userEditAction($id) {
        $em = $this->getDoctrine()->getManager();
        $user = $em->find('AppUserBundle:User', $id);
//        $user = $this->get('security.context')->getToken()->getUser();
        return $this->render('AppUserBundle:User:edit.html.twig', array(
            'user' => $user
        ));
    }

}
