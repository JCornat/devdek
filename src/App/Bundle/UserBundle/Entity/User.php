<?php

namespace App\Bundle\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="App\Bundle\UserBundle\Entity\UserRepository")
 */
class User extends BaseUser {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar", type="string", length=255, nullable=true)
     */
    private $avatar;

    /**
     * @ORM\OneToMany(targetEntity="App\Bundle\BlogBundle\Entity\Article", mappedBy="author")
     */
    private $articles;

    /**
     * @return string
     */
    public function getAvatar() {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     * @return $this;
     */
    public function setAvatar($avatar) {
        $this->avatar = $avatar;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getArticles() {
        return $this->articles;
    }

    /**
     * @param mixed $articles
     * @return $this;
     */
    public function setArticles($articles) {
        $this->articles = $articles;
        return $this;
    }

}
