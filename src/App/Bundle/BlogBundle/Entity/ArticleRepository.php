<?php

namespace App\Bundle\BlogBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Tools\Pagination\Paginator;

class ArticleRepository extends EntityRepository {

    public function getArticles($page, $nbPerPage){
        $query = $this->createQueryBuilder('a')
            ->leftJoin('a.author', 'author')
            ->leftJoin('a.category', 'category')
            ->addSelect('author')
            ->addSelect('category')
            ->orderBy('a.date','DESC');

        $query->setFirstResult(($page - 1)*$nbPerPage)
            ->setMaxResults($nbPerPage);
        return new Paginator($query);
    }

    public function getArticle($id){
        $query = $this->createQueryBuilder('a')
            ->leftJoin('a.tags', 'tag')
            ->leftJoin('a.author', 'author')
            ->addSelect('tag')
            ->addSelect('author')
            ->orderBy('a.date','DESC')
            ->where('a.id = :id')
            ->orWhere('a.slug = :slug')
            ->setParameter(':id', $id)
            ->setParameter(':slug', $id)
            ->getQuery();

        return $query->getSingleResult();
    }

}
