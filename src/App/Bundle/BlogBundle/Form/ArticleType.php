<?php

namespace App\Bundle\BlogBundle\Form;

use App\Bundle\BlogBundle\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Tests\Fixtures\Author;
use Symfony\Component\Form\Tests\Fixtures\AuthorType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ArticleType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('title', 'text')
            ->add('date', 'datetime')
            ->add('summary', 'textarea')
            ->add('content', 'textarea')
            ->add('published', 'checkbox')
            ->add('author', 'entity', array(
                'class' => 'AppUserBundle:User',
                'property' => 'username'
            ))
            ->add('category', 'entity', array(
                'class' => 'AppBlogBundle:Category',
                'property' => 'name'
            ))
//            ->add('tags', 'entity', array(
//                'class' => 'AppBlogBundle:Tag',
//                'property' => 'name',
//                'multiple' => true,
//                'expanded' => true
//            ))
            ->add('tags', 'collection', array(
                'type' => new TagType(),
                'allow_add' => true,
                'allow_delete' => true
            ))
            ->add('save', 'submit');
    }
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Bundle\BlogBundle\Entity\Article'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_bundle_blogbundle_article';
    }
}
