<?php

namespace App\Bundle\BlogBundle\Controller;

use App\Bundle\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BlogController extends Controller {

    public function indexAction($page = 1) {
        if($page < 1) {
            throw $this->createNotFoundException("La page demandée n'existe pas");
        }

        $nbPerPage = 10;
        $articles = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBlogBundle:Article')
            ->getArticles($page, $nbPerPage);

        $nbPages = ceil(count($articles)/$nbPerPage);
        $content = $this->get('templating')
            ->render('AppBlogBundle:Blog:index.html.twig', array(
                'articles' => $articles,
                'page' => $page,
                'nbPages' => $nbPages
            ));
        return new Response($content);
    }

    public function articleAction($id) {
        $article = $this->getDoctrine()->getManager()->getRepository('AppBlogBundle:Article')->getArticle($id);
        $content = $this->get('templating')
            ->render('AppBlogBundle:Article:article.html.twig', array('article' => $article));
        return new Response($content);
    }

    public function aProposAction() {
        $content = $this->get('templating')
            ->render('AppBlogBundle:Article:a-propos.html.twig');
        return new Response($content);
    }

    public function searchAction(Request $request) {
        $query = $request->query->get('query');
        $category = $request->query->get('type');
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('AppBlogBundle:Article')->createQueryBuilder('a')
            ->leftJoin('a.author', 'author')
            ->leftJoin('a.category', 'category')
            ->leftJoin('a.tags', 'tag')
            ->addSelect('author')
            ->addSelect('category')
            ->addSelect('tag')
        ;

        if(!$category || !in_array($category, array('articles', 'categories', 'content', 'tags'))) {
            $category = 'articles';
        }

        switch($category) {
            case 'articles':
                $qb->where('a.title LIKE :query')
                    ->setParameter('query', '%'.$query.'%')
                    ->orWhere('a.summary LIKE :query')
                    ->setParameter('query', '%'.$query.'%')
                ;
                break;
            case 'categories':
                $qb->where('category.name LIKE :query')
                    ->setParameter('query', '%'.$query.'%');
                break;
            case 'tags':
                $qb->where('tag.name LIKE :query')
                    ->setParameter('query', '%'.$query.'%');
                break;
        }

        $articles = $qb->getQuery()->getResult();
        $content = $this->get('templating')
            ->render('AppBlogBundle:Blog:search.html.twig', array(
                'articles' => $articles
            ));
        return new Response($content);
    }

}
