<?php

namespace App\Bundle\BlogBundle\Controller;

use App\Bundle\BlogBundle\Entity\Article;
use App\Bundle\BlogBundle\Entity\Category;
use App\Bundle\BlogBundle\Entity\Member;
use App\Bundle\BlogBundle\Form\ArticleType;
use App\Bundle\BlogBundle\Form\CategoryType;
use App\Bundle\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\Constraints\DateTime;

class AdminController extends Controller {


    public function administrationAction() {
        return $this->render('AppBlogBundle:Admin:index.html.twig');
    }

    public function articleAddAction(Request $request) {

        if(!$this->get('security.context')->isGranted('ROLE_AUTHOR')) {
            throw new AccessDeniedException('Accès limité aux auteurs');
        }

        $em = $this->getDoctrine()->getManager();
        $article = new Article;
        $form = $this->createForm(new ArticleType(), $article);
        $form->handleRequest($request);

        if($form->isValid()) {
            $em->persist($article);
            $em->flush();
            $request->getSession()->getFlashBag()->add('notice','Article enregistré');
            return $this->redirect($this->generateUrl('blog_article', array('id' => $article->getSlug())));
        }

        return $this->render('AppBlogBundle:Article:add.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function articleEditAction($id, Request $request) {

        if(!$this->get('security.context')->isGranted('ROLE_AUTHOR')) {
            throw new AccessDeniedException('Accès limité aux auteurs');
        }

        $em = $this->getDoctrine()->getManager();
        $article = $em->find('AppBlogBundle:Article', $id);
        $form = $this->createForm(new ArticleType(), $article);
        $form->handleRequest($request);

        if($form->isValid()) {
            $em->persist($article);
            $em->flush();
            $request->getSession()->getFlashBag()->add('notice','Article enregistré');
            return $this->redirect($this->generateUrl('blog_article', array('id' => $article->getSlug())));
        }

        return $this->render('AppBlogBundle:Article:add.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function categoryAddAction(Request $request) {

        if(!$this->get('security.context')->isGranted('ROLE_AUTHOR')) {
            throw new AccessDeniedException('Accès limité aux auteurs');
        }

        $category = new Category();
        $form = $this->createForm(new CategoryType(), $category);
        $form->handleRequest($request);

        if($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
            $request->getSession()->getFlashBag()->add('notice','Catégorie enregistrée');
            return $this->redirect($this->generateUrl('blog_article_add'));
        }

        return $this->render('AppBlogBundle:Category:add.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function categoryEditAction($id, Request $request) {

        if(!$this->get('security.context')->isGranted('ROLE_AUTHOR')) {
            throw new AccessDeniedException('Accès limité aux auteurs');
        }

        $em = $this->getDoctrine()->getManager();
        $category = $em->find('AppBlogBundle:Category', $id);
        $form = $this->createForm(new CategoryType(), $category);

        $form->handleRequest($request);

        if($form->isValid()) {
            $em->persist($category);
            $em->flush();
            $request->getSession()->getFlashBag()->add('notice','Catégorie enregistré');
            return $this->redirect($this->generateUrl('blog_home'));
        }

        return $this->render('AppBlogBundle:Category:add.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function categoryListAction() {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository('AppBlogBundle:Category')->findAll();
        return $this->render('AppBlogBundle:Category:list.html.twig', array(
            'categories' => $categories
        ));
    }

    public function userListAction() {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('AppUserBundle:User')->findAll();
        return $this->render('AppUserBundle:User:list.html.twig', array(
            'users' => $users
        ));
    }

}
